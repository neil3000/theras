import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

import DonateIcon from "@components/icons/DonateIcon";

const useStyles = makeStyles(() => ({
  button: {
    background: "linear-gradient(315deg, #6617cb 0%, #cb218e 74%)",
    borderRadius: 3,
    border: 0,
    color: "white",
    height: 48,
    padding: "0 30px",
    boxShadow: "0 3px 5px 2px rgba(102, 23, 203, .3)",
  },
}));

const DonateButton = (props) => {
  const classes = useStyles();

  return (
    <Button className={classes.button} startIcon={<DonateIcon />} {...props} />
  );
};

export default DonateButton;
