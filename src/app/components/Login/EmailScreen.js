import { useState, Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import { useSnackbar } from "notistack";

import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Collapse from "@material-ui/core/Collapse";

import useTranslation from "@helpers/hooks/useTranslation";
import RCAlert from "@components/core/RCAlert";
import { useRc } from "@helpers/hooks/useRc";
import firebase from "@utils/firebase";

const EmailScreen = ({ registering, setRegistering, handleClose }) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const { rc } = useRc();

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const emailRegisterEnabled_accounts =
    rc.emailRegisterEnabled_accounts === "true";
  const [errorPassword, setErrorPassword] = useState(null);
  const [errorPasswordConfirm, setErrorPasswordConfirm] = useState(null);
  const [errorEmail, setErrorEmail] = useState(null);
  const [errorUsername, setErrorUsername] = useState(null);
  const [snackId, setSnackId] = useState(null);

  useEffect(() => {
    closeSnackbar(snackId);
    setSnackId(null);

    if (registering) {
      setErrorPassword(null);
      if (errorEmail == "UserNotFound.Short") setErrorEmail(null);
    } else if (errorEmail == "EmailAlreadyUsed.Short") setErrorEmail(null);
  }, [registering]);

  const handleEmailSignIn = async (e) => {
    e.preventDefault();

    setErrorPassword(null);
    setErrorPasswordConfirm(null);
    setErrorEmail(null);

    if (registering) {
      if (e.target.pwd.value !== e.target.pwdConfirm.value) {
        setErrorPasswordConfirm("PasswordsDontMatch");
      } else if (e.target.username.value == "") {
        setErrorUsername("InvalidUsername");
      } else {
        firebase
          .auth()
          .createUserWithEmailAndPassword(
            e.target.email.value,
            e.target.pwd.value
          )
          .then(async (data) => {
            firebase.analytics().logEvent("sign_up", { provider: "email" });

            data.user
              .updateProfile({
                displayName: e.target.username.value,
              })
              .then(
                function () {
                  data.user.sendEmailVerification().then(
                    function () {
                      enqueueSnackbar(
                        t("Login:EmailScreen.Success.First", {
                          name: e.target.username.value,
                        }),
                        { variant: "success" }
                      );
                    },
                    function (error) {
                      enqueueSnackbar(t("Login:EmailScreen.Errors.Unknown"), {
                        variant: "error",
                      });
                      console.log(error);
                    }
                  );
                },
                function (error) {
                  enqueueSnackbar(t("Login:EmailScreen.Errors.Unknown"), {
                    variant: "error",
                  });
                  console.log(error);
                }
              );

            handleClose();
          })
          .catch((error) => {
            if (error.code == "auth/email-already-in-use") {
              setErrorEmail("EmailAlreadyUsed.Short");

              setSnackId(
                enqueueSnackbar(
                  t("Login:EmailScreen.Errors.EmailAlreadyUsed.Long"),
                  {
                    variant: "info",
                    autoHideDuration: 5000,
                    action: (
                      <Fragment>
                        <Button
                          color="inherit"
                          size="small"
                          onClick={() => {
                            setRegistering(false);
                          }}
                        >
                          {t("Login:EmailScreen.Actions.SignIn")}
                        </Button>
                      </Fragment>
                    ),
                  }
                )
              );
            } else if (error.code == "auth/invalid-email") {
              setErrorEmail("InvalidEmail");
            } else if (error.code == "auth/weak-password") {
              setErrorPassword("WeakPassword");
            } else {
              enqueueSnackbar(t("Login:EmailScreen.Errors.Unknown"), {
                variant: "error",
              });
              console.log(error);
            }
          });
      }
    } else {
      firebase
        .auth()
        .signInWithEmailAndPassword(e.target.email.value, e.target.pwd.value)
        .then(() => {
          firebase.analytics().logEvent("login", { provider: "email" });
          enqueueSnackbar(
            t("Login:EmailScreen.Success.Back", {
              name: e.target.username.value,
            }),
            { variant: "success" }
          );
          handleClose();
        })
        .catch((error) => {
          if (error.code == "auth/invalid-email") {
            setErrorEmail("InvalidEmail");
          } else if (error.code == "auth/user-disabled") {
            setErrorEmail("UserDisabled");
          } else if (error.code == "auth/user-not-found") {
            setErrorEmail("UserNotFound.Short");

            setSnackId(
              enqueueSnackbar(t("Login:EmailScreen.Errors.UserNotFound.Long"), {
                variant: "info",
                autoHideDuration: 5000,
                action: (
                  <Fragment>
                    <Button
                      color="inherit"
                      size="small"
                      onClick={() => {
                        setRegistering(true);
                      }}
                    >
                      {t("Login:EmailScreen.Actions.Register")}
                    </Button>
                  </Fragment>
                ),
              })
            );
          } else if (error.code == "auth/wrong-password") {
            setErrorPassword("WrongPassword");
          } else {
            enqueueSnackbar(t("Login:EmailScreen.Errors.Unknown"), {
              variant: "error",
            });
            console.log(error);
          }
        });
    }
  };

  return (
    <>
      <RCAlert rc="accountEmail" className={classes.alert} />
      <form id="signInForm" noValidate onSubmit={handleEmailSignIn}>
        <Grid direction="column" style={{ textAlign: "center" }} container>
          <Grid item>
            <TextField
              name="email"
              label={t("Login:EmailScreen.Inputs.Email")}
              type="email"
              helperText={
                errorEmail && t("Login:EmailScreen.Errors." + errorEmail)
              }
              error={Boolean(errorEmail)}
              onChange={() => {
                setErrorEmail(null);
              }}
              autoComplete="email"
            />
          </Grid>
          <Grid item className={classes.marginTopOne}>
            <TextField
              name="pwd"
              label={t("Login:EmailScreen.Inputs.Password")}
              type="password"
              helperText={
                errorPassword && t("Login:EmailScreen.Errors." + errorPassword)
              }
              error={Boolean(errorPassword)}
              onChange={() => {
                setErrorPassword(null);
                setErrorPasswordConfirm(null);
              }}
              autoComplete={registering ? "new-password" : "current-password"}
            />
          </Grid>
          <Collapse in={!registering}>
            <Grid item className={classes.marginTopOne}>
              <Button
                size="small"
                className={classes.newAccButton}
                onClick={() => {
                  setRegistering(true);
                }}
                disabled={!emailRegisterEnabled_accounts}
                variant={errorEmail == "UserNotFound" ? "contained" : "text"}
                color={errorEmail == "UserNotFound" ? "primary" : "default"}
                disableElevation
              >
                {t("Login:EmailScreen.Actions.DontHaveAnAccount?")}
              </Button>
            </Grid>
          </Collapse>
          <Collapse in={registering}>
            <Grid item className={classes.marginTopOne}>
              <TextField
                name="pwdConfirm"
                label={t("Login:EmailScreen.Inputs.ConfirmPassword")}
                type="password"
                helperText={
                  errorPasswordConfirm &&
                  t("Login:EmailScreen.Errors." + errorPasswordConfirm)
                }
                error={Boolean(errorPasswordConfirm)}
                onChange={() => {
                  setErrorPassword(null);
                  setErrorPasswordConfirm(null);
                }}
                autoComplete={registering ? "new-password" : "current-password"}
              />
            </Grid>
            <Grid item className={classes.marginTopOne}>
              <TextField
                name="username"
                label={t("Login:EmailScreen.Inputs.Username")}
                type="text"
                helperText={
                  errorUsername &&
                  t("Login:EmailScreen.Errors." + errorUsername)
                }
                error={Boolean(errorUsername)}
                onChange={() => {
                  setErrorUsername(null);
                }}
                autoComplete="username"
              />
            </Grid>
          </Collapse>
        </Grid>
      </form>
    </>
  );
};

const useStyles = makeStyles((theme) => ({
  marginTopOne: {
    marginTop: theme.spacing(2),
  },
  newAccButton: {
    marginTop: theme.spacing(2),
    textTransform: "none",
  },
  alert: {
    marginBottom: theme.spacing(2),
  },
}));

EmailScreen.propTypes = {
  registering: PropTypes.bool.isRequired,
  setRegistering: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
};
export default EmailScreen;
