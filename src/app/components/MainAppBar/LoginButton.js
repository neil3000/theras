import React from "react";

import LoginIcon from "../icons/LoginIcon";
import IconButtonLink from "@components/core/IconButtonLink";

import useTranslation from "@helpers/hooks/useTranslation";

const LoginButton = () => {
  const { t } = useTranslation();

  return (
    <IconButtonLink
      title={t("MainAppBar:LoginButton.Tooltip")}
      aria-label={t("MainAppBar:LoginButton.AriaLabel")}
      aria-controls="modal-login"
      aria-haspopup="true"
      color="inherit"
      href="/login"
    >
      <LoginIcon />
    </IconButtonLink>
  );
};

export default LoginButton;
