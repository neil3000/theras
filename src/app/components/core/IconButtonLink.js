import React from "react";
import PropTypes from "prop-types";
import Link from "next/link";

import IconButton from "@components/core/IconButton";

const IconButtonLink = ({ href, ...other }) => {
  return (
    <Link href={href}>
      <IconButton {...other} />
    </Link>
  );
};

IconButtonLink.propTypes = {
  href: PropTypes.string,
};
export default IconButtonLink;
