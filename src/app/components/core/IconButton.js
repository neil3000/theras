import React from "react";
import PropTypes from "prop-types";

import IconButtonMat from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";

const IconButton = ({ title, ...other }) => {
  return (
    <Tooltip title={title}>
      <IconButtonMat {...other} />
    </Tooltip>
  );
};

IconButton.propTypes = {
  title: PropTypes.string,
};
export default IconButton;
