import React from "react";
import PropTypes from "prop-types";
import firebase from "firebase";

const RCContext = React.createContext({});

const defaults = {
  account_bannerAlert: "",
  accountEmail_bannerAlert: "",
  googleEnabled_accounts: "false",
  emailEnabled_accounts: "false",
  appbarButtonVisible_accounts: "false",
  emailRegisterEnabled_accounts: "false",
  appbarDonateFabExtended_donations: "true",
  headerSection:
    '[{"title":{"en":"Fight against deforestation"},"desc":{"en":"Because trees are cool","fr":"Parce que les arbres c\'est stylax"},"button":{"type":"donate","text":{"en":"Donate now","fr":"Faire un don"}},"imgUrl":"https://www.minecraft.net/content/dam/games/minecraft/screenshots/taking%20inventory-sapling-header.jpg"},{"title":{"en":"Be part of a community","fr":"Faites partie d\'une communaut\u00E9"},"desc":{"en":"Make new friends","fr":"Fais toi de nouveaux poteaux"},"button":{"type":"discord","text":{"en":"Join us","fr":"Rejoins-nous"}},"imgUrl":"https://cdn.pixabay.com/photo/2017/02/10/00/03/minecraft-2053882_960_720.jpg"}]',
};

export function RCProvider({ children }) {
  const [rc, setRc] = React.useState(defaults);

  React.useEffect(() => {
    const remoteConfig = firebase.remoteConfig();
    remoteConfig.settings.minimumFetchIntervalMillis = 36000;
    remoteConfig.defaultConfig = defaults;
    remoteConfig
      .fetchAndActivate()
      .then(() => {
        console.log(remoteConfig.getAll());
        return remoteConfig.getAll();
      })
      .then((remoteFlags) => {
        const newFlags = {
          ...rc,
        };

        for (const [key, config] of Object.entries(remoteFlags)) {
          newFlags[key] = config.asString();
        }

        setRc(newFlags);
      })
      .catch((error) => console.error(error));
  }, []);

  return <RCContext.Provider value={{ rc }}>{children}</RCContext.Provider>;
}
RCProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useRc = () => {
  return React.useContext(RCContext);
};
