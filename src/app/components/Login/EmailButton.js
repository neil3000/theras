import React from "react";
import PropTypes from "prop-types";

import useTranslation from "@helpers/hooks/useTranslation";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Email } from "@material-ui/icons";

import { useRc } from "@helpers/hooks/useRc";

const EmailButton = ({ setUsingEmail }) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const { rc } = useRc();
  const emailEnabled_accounts = rc.emailEnabled_accounts === "true";

  return (
    <Button
      onClick={() => {
        setUsingEmail(true);
      }}
      className={classes.emailButton}
      startIcon={<Email />}
      disabled={!emailEnabled_accounts}
    >
      {t("Login:EmailButton.Actions.OrEmail")}
    </Button>
  );
};

const useStyles = makeStyles(() => ({
  emailButton: {
    textTransform: "none",
  },
}));

EmailButton.propTypes = {
  setUsingEmail: PropTypes.func.isRequired,
};
export default EmailButton;
