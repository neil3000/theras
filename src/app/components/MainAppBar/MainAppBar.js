import React from "react";
import PropTypes from "prop-types";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { makeStyles } from "@material-ui/core/styles";
import Slide from "@material-ui/core/Slide";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";

import { useRc } from "@helpers/hooks/useRc";
import { useAuth } from "@helpers/hooks/useAuth";
import LoginButton from "@components/MainAppBar/LoginButton";
import UserButton from "@components/MainAppBar/UserButton";
import ThemeButton from "@components/MainAppBar/ThemeButton";
import LanguagePickerButton from "@components/MainAppBar/LanguagePickerButton";
import DonateFab from "@components/MainAppBar/DonateFab";

function HideOnScroll({ children, window }) {
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: 600,
  });
  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}
HideOnScroll.propTypes = {
  children: PropTypes.object.isRequired,
  window: PropTypes.object,
};

const useStyles = makeStyles((theme) => ({
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const MainAppBar = () => {
  const classes = useStyles();
  const { user } = useAuth();
  const { rc } = useRc();
  const appbarButtonVisible_accounts =
    rc.appbarButtonVisible_accounts === "true";

  return (
    <>
      <HideOnScroll>
        <AppBar>
          <Toolbar>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Theras
            </Typography>

            <LanguagePickerButton />

            <ThemeButton />

            {appbarButtonVisible_accounts &&
              (user ? <UserButton /> : <LoginButton />)}
          </Toolbar>
        </AppBar>
      </HideOnScroll>
      <DonateFab />
    </>
  );
};

export default MainAppBar;
