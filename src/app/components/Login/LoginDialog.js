import React from "react";
import PropTypes from "prop-types";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import Collapse from "@material-ui/core/Collapse";
import { makeStyles } from "@material-ui/core/styles";

import useTranslation from "@helpers/hooks/useTranslation";
import ProvidersScreen from "./ProvidersScreen";
import EmailScreen from "./EmailScreen";

const LoginDialog = ({ handleClose, open }) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const [usingEmail, setUsingEmail] = React.useState(false);
  const [registering, setRegistering] = React.useState(false);

  const handleBack = () => {
    usingEmail
      ? registering
        ? setRegistering(false)
        : setUsingEmail(false)
      : handleClose();
  };

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleBack}
      aria-labelledby={t("Login:LoginDialog.AriaLabel")}
      aria-describedby={t("Login:LoginDialog.AriaDescription")}
    >
      <DialogTitle>{t("Login:LoginDialog.Title")}</DialogTitle>
      <DialogContent className={classes.root}>
        <Collapse in={!usingEmail}>
          <ProvidersScreen
            setUsingEmail={setUsingEmail}
            handleClose={handleClose}
          />
        </Collapse>
        <Collapse in={usingEmail}>
          <EmailScreen
            registering={registering}
            setRegistering={setRegistering}
            handleClose={handleClose}
          />
        </Collapse>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleBack} color="primary">
          {t("Login:LoginDialog.Actions." + (usingEmail ? "Back" : "Cancel"))}
        </Button>
        {usingEmail && (
          <Button form="signInForm" type="submit" color="primary">
            {t(
              "Login:LoginDialog.Actions." +
                (registering ? "Register" : "SignIn")
            )}
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    paddingBottom: theme.spacing(2),
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

LoginDialog.propTypes = {
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};
export default LoginDialog;
