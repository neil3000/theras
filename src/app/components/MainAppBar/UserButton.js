import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

import IconButton from "@components/core/IconButton";
import firebase from "@utils/firebase";
import useTranslation from "@helpers/hooks/useTranslation";
import { useAuth } from "@helpers/hooks/useAuth";

const useStyles = makeStyles((theme) => ({
  small: {
    width: theme.spacing(4),
    height: theme.spacing(4),
  },
}));

const UserButton = () => {
  const classes = useStyles();
  const [userAnchorEl, setUserAnchorEl] = React.useState(null);
  const { t } = useTranslation();
  const { user } = useAuth();

  const openUserMenu = (event) => {
    setUserAnchorEl(event.currentTarget);
  };

  const closeUserMenu = () => {
    setUserAnchorEl(null);
  };

  const signOut = () => {
    firebase.analytics().logEvent("logout");
    firebase.auth().signOut();
    setUserAnchorEl(null);
  };

  return (
    <>
      <IconButton
        title={t("MainAppBar:UserButton.Tooltip") + user.displayName}
        aria-label={t("MainAppBar:UserButton.AriaLabel")}
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={openUserMenu}
        color="inherit"
      >
        <Avatar
          alt={user.displayName}
          src={user.photoURL}
          className={classes.small}
        />
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={userAnchorEl}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={userAnchorEl != null}
        onClose={closeUserMenu}
      >
        <MenuItem onClick={closeUserMenu}>Profile</MenuItem>
        <MenuItem onClick={signOut}>Sign out</MenuItem>
      </Menu>
    </>
  );
};

export default UserButton;
