export default {
  en: {
    common: require("./en/common.json"),
    Login: require("./en/Login.json"),
    MainAppBar: require("./en/MainAppBar.json"),
  },
  fr: {
    common: require("./fr/common.json"),
  },
};
