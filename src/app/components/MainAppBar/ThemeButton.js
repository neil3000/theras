import React from "react";
import { useSnackbar } from "notistack";

import Brightness4Icon from "@material-ui/icons/Brightness4";
import Brightness7Icon from "@material-ui/icons/Brightness7";

import IconButton from "@components/core/IconButton";
import useTranslation from "@helpers/hooks/useTranslation";
import { useTheme } from "@helpers/hooks/useTheme";

const ThemeButton = () => {
  const { t } = useTranslation();
  const { theme, setTheme } = useTheme();
  const { enqueueSnackbar } = useSnackbar();

  return (
    <IconButton
      title={t("MainAppBar:ThemeButton.Tooltip." + (theme ? "Light" : "Dark"))}
      aria-label={t(
        "MainAppBar:ThemeButton.AriaLabel" + (theme ? "Light" : "Dark")
      )}
      aria-controls="theme"
      aria-haspopup="false"
      color="inherit"
      onClick={() => {
        setTheme(!theme);
        enqueueSnackbar(
          t("MainAppBar:ThemeButton.Confirmation." + (theme ? "Light" : "Dark"))
        );
      }}
    >
      {theme ? <Brightness7Icon /> : <Brightness4Icon />}
    </IconButton>
  );
};

export default ThemeButton;
