import React from "react";
import PropTypes from "prop-types";

import Tooltip from "@material-ui/core/Tooltip";
import Button from "@material-ui/core/Button";

const TooltipButton = (props) => {
  var { title, ...other } = props;

  return (
    <Tooltip title={title} enterDelay={750}>
      <Button {...other} />
    </Tooltip>
  );
};

TooltipButton.propTypes = {
  title: PropTypes.string.isRequired,
};

export default TooltipButton;
