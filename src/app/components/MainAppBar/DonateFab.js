import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import Tooltip from "@material-ui/core/Tooltip";
import Zoom from "@material-ui/core/Zoom";
import Slide from "@material-ui/core/Slide";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";

import { useRc } from "@helpers/hooks/useRc";
import useTranslation from "@helpers/hooks/useTranslation";
import DonateIcon from "@components/icons/DonateIcon";

const useStyles = makeStyles((theme) => ({
  donateFab: {
    position: "fixed",
    bottom: theme.spacing(3),
    right: theme.spacing(3),
    zIndex: 2,
    background: "linear-gradient(315deg, #6617cb 0%, #cb218e 74%)",
    border: 0,
    color: "white",
    boxShadow: "0 3px 5px 2px rgba(102, 23, 203, .3)",
  },
  fabText: {
    marginLeft: theme.spacing(1),
  },
}));

const DonateFab = ({ window }) => {
  const classes = useStyles();
  const { rc } = useRc();
  const { t } = useTranslation();
  const appbarDonateFabExtended_donations =
    rc.appbarDonateFabExtended_donations === "true";

  const appearTrigger = useScrollTrigger({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: 200,
  });

  return appbarDonateFabExtended_donations ? (
    <Slide in={appearTrigger} direction="up">
      <Fab
        variant="extended"
        className={classes.donateFab}
        aria-label={t("MainAppBar:DonateFab.AriaLabel")}
      >
        <DonateIcon />

        <span className={classes.fabText}>
          {t("MainAppBar:DonateFab.Actions.Donate")}
        </span>
      </Fab>
    </Slide>
  ) : (
    <Zoom in={appearTrigger}>
      <Tooltip title={t("MainAppBar:DonateFab.Tooltip")}>
        <Fab
          variant="round"
          className={classes.donateFab}
          aria-label={t("MainAppBar:DonateFab.AriaLabel")}
        >
          <DonateIcon />
        </Fab>
      </Tooltip>
    </Zoom>
  );
};

DonateFab.propTypes = {
  window: PropTypes.object,
};
export default DonateFab;
