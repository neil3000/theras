import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  root: {
    position: "fixed",
    right: 0,
    bottom: 0,
    minWidth: "100%",
    minHeight: "100%",
    zIndex: -1,
  },
}));

const BgVideo = (props) => {
  const classes = useStyles();

  return (
    <video className={classes.root} autoPlay muted loop>
      <source src={props.src} type="video/mp4" />
      Your browser does not support the video tag.
    </video>
  );
};

BgVideo.propTypes = {
  src: PropTypes.string,
};
export default BgVideo;
