import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

//import { useRc } from "@helpers/hooks/useRc";
import DonateButton from "@components/core/DonateButton";
import useTranslation from "@helpers/hooks/useTranslation";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    marginRight: theme.spacing(4),
    marginLeft: theme.spacing(-4),
  },
  button: {
    marginTop: theme.spacing(4),
  },
  slideText: {
    color: theme.palette.common.white,
  },
}));

const SlideData = ({ fake, disabled, float, data }) => {
  const classes = useStyles();
  const { locale } = useTranslation();

  return (
    <Grid
      container
      alignItems="center"
      direction="column"
      justify="center"
      style={
        float
          ? {
              position: "absolute",
              top: 0,
              left: 0,
              opacity: fake ? 0 : 1,
            }
          : { opacity: fake ? 0 : 1 }
      }
      className={classes.root}
    >
      <Grid item className={classes.slideText}>
        <Typography variant="h5" component="h2" gutterBottom color="inherit">
          {data.title[locale] || data.title.en}
        </Typography>
      </Grid>
      <Grid item className={classes.slideText}>
        <Typography variant="body1" color="inherit" component="p">
          {data.desc[locale] || data.desc.en}
        </Typography>
      </Grid>
      <Grid item className={classes.button}>
        <DonateButton disabled={fake || disabled}>
          {data.button.text[locale] || data.button.text.en}
        </DonateButton>
      </Grid>
    </Grid>
  );
};

SlideData.propTypes = {
  fake: PropTypes.boolean,
  disabled: PropTypes.boolean,
  float: PropTypes.boolean,
  data: PropTypes.object.required,
};
export default SlideData;
