import React from 'react'
import { useRouter } from 'next/router'
import firebase from 'firebase'

import withHead from '@helpers/hocs/withHead';
import useTranslation from '@helpers/hooks/useTranslation'
import HeaderSection from '@components/HeaderSection/HeaderSection'
import MainAppBar from '@components/MainAppBar/MainAppBar'
import LoginDialog from '@components/Login/LoginDialog'

const Page = () => {
  const router = useRouter()
  const { t } = useTranslation();
  const { page } = router.query

  React.useEffect(() => {
    if (!page || ['/', 'login'].indexOf(page[0])>=0) {
      firebase.analytics().logEvent("page_view", {page_path: page&&"/"});
    }else{
      router.push('/');
    }
  }, [page]);

  return (
    <>
      <MainAppBar/>
      <LoginDialog open={page=='login'} handleClose={() => router.push('/', '', { shallow: true })}/>
      
      <HeaderSection/>
      <div style={{width: '100%', height: '100vh'}} />
      <div style={{width: '100%', height: '100vh'}} />
    </>
  )
}

export default withHead(Page, ['all']);