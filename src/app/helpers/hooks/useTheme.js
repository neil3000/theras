import React from "react";
import PropTypes from "prop-types";

import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles/";

const RCContext = React.createContext({});

const themeLight = createMuiTheme({
  palette: {
    primary: {
      main: "#6b54b6",
    },
    secondary: {
      main: "#120a17",
    },
  },
});

const themeDark = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      main: "#6b54b6",
    },
    secondary: {
      main: "#120a17",
    },
    background: {
      default: "#1b0f23",
      paper: "#1e1e1e",
    },
  },
});

export function ThemeProvider({ children }) {
  const [theme, setTheme] = React.useState(false);

  React.useEffect(() => {
    setTheme(false);
  }, []);

  return (
    <RCContext.Provider value={{ theme: theme, setTheme: setTheme }}>
      <MuiThemeProvider theme={theme ? themeDark : themeLight}>
        {children}
      </MuiThemeProvider>
    </RCContext.Provider>
  );
}
ThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useTheme = () => {
  return React.useContext(RCContext);
};
