import config from './config';
import firebase from 'firebase/app';
import 'firebase/functions';
import 'firebase/auth';
// import 'firebase/firestore' // Soon
// import 'firebase/storage' // Maybe?
import 'firebase/analytics';
import 'firebase/remote-config'

const clientCredentials = {
  development: {
    apiKey: '__________',
    authDomain: 'PROJECT_ID_DEV.firebaseapp.com',
    databaseURL: 'https://PROJECT_ID_DEV.firebaseio.com',
    projectId: 'PROJECT_ID_DEV',
    storageBucket: 'PROJECT_ID_DEV.appspot.com',
    messagingSenderId: '__________',
  },

  staging: {
    apiKey: '__________',
    authDomain: 'PROJECT_ID_STAGING.firebaseapp.com',
    databaseURL: 'https://PROJECT_ID_STAGING.firebaseio.com',
    projectId: 'PROJECT_ID_STAGING',
    storageBucket: 'PROJECT_ID_STAGING.appspot.com',
    messagingSenderId: '__________',
  },

  production: {
    apiKey: "AIzaSyAyTmxITPl_bFupBV5ks6ABzYo7eA84K-8",
    authDomain: "rahneil-n3-theras.firebaseapp.com",
    projectId: "rahneil-n3-theras",
    storageBucket: "rahneil-n3-theras.appspot.com",
    messagingSenderId: "700837553884",
    appId: "1:700837553884:web:6b3f8daf7e5330ad7bb619",
    measurementId: "G-Z4KCV05EFH"
  },
};

// Check that `window` is in scope for the analytics module!
if (typeof window !== 'undefined' && !firebase.apps.length) {
  firebase.initializeApp(clientCredentials[config.FIREBASE_ENV]);
  firebase.remoteConfig().fetchAndActivate();
  firebase.analytics()
  if ('measurementId' in clientCredentials) firebase.analytics(); // If dev disabled it to reduce tests work loads?
}

export default firebase;
