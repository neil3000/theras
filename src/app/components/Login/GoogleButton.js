import React from "react";
import PropTypes from "prop-types";
import { useSnackbar } from "notistack";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

import useTranslation from "@helpers/hooks/useTranslation";
import { useRc } from "@helpers/hooks/useRc";
import firebase from "@utils/firebase";

const GoogleButton = ({ handleClose }) => {
  const classes = useStyles();
  const { rc } = useRc();
  const { t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();
  const googleEnabled_accounts = rc.googleEnabled_accounts === "true";

  const signInWithGoogle = () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase
      .auth()
      .signInWithPopup(provider)
      .then(async (data) => {
        firebase
          .analytics()
          .logEvent(data.additionalUserInfo.isNewUser ? "sign_up" : "login", {
            provider: data.additionalUserInfo.providerId,
          });

        const tpath =
          "Login:GoogleButton.Success." +
          (data.additionalUserInfo.isNewUser ? "First" : "Back");
        enqueueSnackbar(
          t(tpath + ".1") + data.user.displayName + t(tpath + ".2"),
          { variant: "success" }
        );
        handleClose();
      })
      .catch((error) => {
        if (error.code == "auth/account-exists-with-different-credential") {
          enqueueSnackbar(t("Login:GoogleButton.Errors.AccountExists"), {
            variant: "error",
          });
        } else if (error.code == "auth/popup-blocked") {
          enqueueSnackbar(t("Login:GoogleButton.Errors.PopupBlocked"), {
            variant: "error",
          });
        } else if (error.code == "auth/popup-closed-by-user") {
          enqueueSnackbar(t("Login:GoogleButton.Errors.Cancelled"), {
            variant: "warning",
          });
        } else {
          enqueueSnackbar(t("Login:GoogleButton.Errors.Unknown"), {
            variant: "error",
          });
          console.log(error);
        }
      });
  };

  return (
    <Button
      size="large"
      variant="contained"
      onClick={signInWithGoogle}
      className={classes.googleButton}
      startIcon={
        <img
          className={classes.googleLogo}
          src="https://lh3.googleusercontent.com/COxitqgJr1sJnIDe8-jiKhxDx1FrYbtRHKJ9z_hELisAlapwE9LUPh6fcXIfb5vwpbMl4xl9H9TRFPc5NOO8Sb3VSgIBrfRYvW6cUA"
        />
      }
      disabled={!googleEnabled_accounts}
    >
      {t("Login:GoogleButton.Actions.SignIn")}
    </Button>
  );
};

const useStyles = makeStyles((theme) => ({
  googleButton: {
    backgroundColor: theme.palette.common.white,
    color: theme.palette.grey[700],
    textTransform: "none",
  },
  googleLogo: {
    width: 24,
    height: 24,
    marginRight: theme.spacing(1),
  },
}));

GoogleButton.propTypes = {
  handleClose: PropTypes.func.isRequired,
};
export default GoogleButton;
