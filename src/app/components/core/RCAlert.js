import React from "react";

import Alert from "@material-ui/lab/Alert";

import { useRc } from "@helpers/hooks/useRc";

const RCalert = (props) => {
  const { rc } = useRc();

  if (rc[props.rc + "_bannerAlert"]) {
    let msg = rc[props.rc + "_bannerAlert"].split("|$|")[0];
    let type = rc[props.rc + "_bannerAlert"].split("|$|")[1] || "warning";

    return msg ? (
      <Alert severity={type} style={props.style} className={props.className}>
        <div dangerouslySetInnerHTML={{ __html: msg }} />
      </Alert>
    ) : null;
  } else {
    return null;
  }
};

export default RCalert;
