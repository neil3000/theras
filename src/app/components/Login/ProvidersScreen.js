import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

import RCAlert from "@components/core/RCAlert";
import EmailButton from "@components/Login/EmailButton";
import GoogleButton from "@components/Login/GoogleButton";

const ProvidersScreen = ({ handleClose, setUsingEmail }) => {
  const classes = useStyles();

  return (
    <>
      <RCAlert rc="account" className={classes.alert} />

      <Grid
        container
        spacing={2}
        justify="center"
        className={classes.gridButtons}
        direction="column"
      >
        <Grid item>
          <GoogleButton handleClose={handleClose} />
        </Grid>
        <Grid item>
          <EmailButton setUsingEmail={setUsingEmail} />
        </Grid>
      </Grid>
    </>
  );
};

const useStyles = makeStyles((theme) => ({
  gridButtons: {
    textAlign: "center",
  },
  alert: {
    marginBottom: theme.spacing(2),
  },
}));

ProvidersScreen.propTypes = {
  handleClose: PropTypes.func.isRequired,
  setUsingEmail: PropTypes.func.isRequired,
};
export default ProvidersScreen;
