import React, { useContext } from "react";
import firebase from "firebase/app";
import "firebase/analytics";
import { LocaleContext } from "@helpers/translation/context";

import { KeyboardArrowDown, Translate } from "@material-ui/icons";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";

import withHead from "@helpers/hocs/withHead";
import useTranslation from "@helpers/hooks/useTranslation";
import TooltipButton from "@components/core/TooltipButton";

const options = ["English", "Français"];
const results = ["en", "fr"];

const LanguagePickerButton = () => {
  const { t } = useTranslation();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const { setLocale } = useContext(LocaleContext);

  const handleClickListItem = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuItemClick = (event, index) => {
    setLocale(results[index]);
    firebase.analytics().logEvent("changedLanguage", {
      RahNeil_language: results[index],
    });
    setAnchorEl(null);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <TooltipButton
        title={t("MainAppBar:LanguagePickerButton.Tooltip")}
        aria-label={t("MainAppBar:LanguagePickerButton.AriaLabel")}
        aria-controls="menu-langage-picker"
        aria-haspopup="true"
        color="inherit"
        startIcon={<Translate />}
        endIcon={<KeyboardArrowDown />}
        onClick={handleClickListItem}
      >
        {t("MainAppBar:LanguagePickerButton.Text")}
      </TooltipButton>
      <Menu
        id="menu-langage-picker"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {options.map((option, index) => (
          <MenuItem
            key={option}
            selected={
              index ===
              options.indexOf(t("MainAppBar:LanguagePickerButton.Text"))
            }
            onClick={(event) => handleMenuItemClick(event, index)}
          >
            {option}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
};

export default withHead(LanguagePickerButton, ["all"]);
