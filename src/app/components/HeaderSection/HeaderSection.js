import React from "react";
import { Parallax } from "react-scroll-parallax";

import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Fade from "@material-ui/core/Fade";

import { useRc } from "@helpers/hooks/useRc";
import SlideData from "@components/HeaderSection/SlideData";
import { useTheme } from "@helpers/hooks/useTheme";

const useStyles = makeStyles((theme) => ({
  root: {
    overflow: "hidden",
    position: "fixed",
  },
  slideImageContainer: {
    flexGrow: 1,
    position: "relative",
  },
  slideImage: {
    position: "absolute",
    top: 0,
    width: "100%",
    height: "100vh",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "40%",
    transform: "scaleX(-1)",
  },
  stoneRepeatContainer: {
    position: "absolute",
    left: 0,
    zIndex: -1,
    width: "100%",
  },
  stoneRepeatDark: {
    backgroundPosition: "left",
    backgroundImage: "url(/static/assets/headerRepeat-dark.png)",
    width: "100%",
    height: "100vh",
    backgroundSize: "contain",
  },
  stoneRepeatLight: {
    backgroundPosition: "left",
    backgroundImage: "url(/static/assets/headerRepeat-light.png)",
    width: "100%",
    height: "100vh",
    backgroundSize: "contain",
  },
  stoneDividerImg: {
    height: "100vh",
  },
  parallaxContainer: {
    position: "absolute",
    width: "100vw",
    left: "100%",
  },
  containerOverhang: {
    height: "100vh",
    width: "fit-content",
    position: "absolute",
    right: "100%",
  },
  slideWrapper: {
    position: "relative",
  },
  transitionDiv: {
    width: "100vw",
    height: "100vh",
  },
  lightDividerWrapper: {
    position: "absolute",
    left: 0,
    top: 0,
  },
  backdrop: {
    width: "100vw",
    height: "100vh",
    backgroundColor: "#0004",
    position: "absolute",
    top: 0,
  },
  smoothWidth: {
    transition: theme.transitions.create(
      "width"
    ) /* Unused currently but maybe later in #34 */,
  },
}));

const HeaderSection = () => {
  const classes = useStyles();
  const { rc } = useRc();
  const { theme } = useTheme();
  const [slide, setSlide] = React.useState(0);
  const delay = 6000;

  React.useEffect(() => {
    setTimeout(
      function () {
        setSlide(1);
      }.bind(this),
      delay
    );
  }, []);

  return (
    <>
      <Grid container className={classes.root} alignItems="stretch">
        <Grid item className={classes.slideImageContainer}>
          <div
            className={classes.slideImage}
            style={{
              backgroundImage:
                "url(" + JSON.parse(rc.headerSection)[0].imgUrl + ")",
            }}
          />
          {JSON.parse(rc.headerSection).map((d, i) => (
            <Fade
              key={"slide_k" + i}
              in={slide == i}
              appear={false}
              onEntered={() =>
                setTimeout(
                  function () {
                    setSlide(
                      i == JSON.parse(rc.headerSection).length - 1
                        ? 0
                        : slide + 1
                    );
                  }.bind(this),
                  delay
                )
              }
            >
              <div
                className={classes.slideImage}
                style={{
                  backgroundImage: "url(" + d.imgUrl + ")",
                }}
              />
            </Fade>
          ))}

          <div className={classes.backdrop} />
        </Grid>
        <Grid item className={classes.smoothWidth}>
          <SlideData data={JSON.parse(rc.headerSection)[slide]} fake />
        </Grid>
        <div className={classes.parallaxContainer}>
          <Parallax x={["100%", "-100%"]}>
            <div className={classes.stoneRepeatContainer}>
              <div className={classes.stoneRepeatDark} />
            </div>
            <div className={classes.stoneRepeatContainer}>
              <Fade in={!theme} appear={false}>
                <div className={classes.stoneRepeatLight} />
              </Fade>
            </div>
            <Grid container zeroMinWidth className={classes.containerOverhang}>
              <Grid item style={{ position: "relative" }}>
                <img
                  src={"/static/assets/headerDivider-dark.png"}
                  className={classes.stoneDividerImg}
                />
                <div className={classes.lightDividerWrapper}>
                  <Fade in={!theme} appear={false}>
                    <img
                      src={"/static/assets/headerDivider-light.png"}
                      className={classes.stoneDividerImg}
                    />
                  </Fade>
                </div>
              </Grid>
              <Grid item className={classes.slideWrapper}>
                <div className={classes.stoneRepeatContainer}>
                  <div className={classes.stoneRepeatDark} />
                </div>
                <div className={classes.stoneRepeatContainer}>
                  <Fade in={!theme} appear={false}>
                    <div className={classes.stoneRepeatLight} />
                  </Fade>
                </div>

                <div
                  className={classes.smoothWidth}
                  style={{ position: "relative" }}
                >
                  <SlideData data={JSON.parse(rc.headerSection)[slide]} fake />

                  {JSON.parse(rc.headerSection).map((d, i) => (
                    <Fade key={"data_k" + i} in={slide == i} appear={false}>
                      <div>
                        <SlideData data={d} float />
                      </div>
                    </Fade>
                  ))}
                </div>
              </Grid>
            </Grid>
            <div className={classes.transitionDiv} />
          </Parallax>
        </div>
      </Grid>
    </>
  );
};

export default HeaderSection;
