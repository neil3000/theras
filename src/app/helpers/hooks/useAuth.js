import React from "react";
import PropTypes from "prop-types";
import nookies from "nookies";

import firebase from "@utils/firebase";

const AuthContext = React.createContext({
  user: null,
});

export function AuthProvider({ children }) {
  const [user, setUser] = React.useState(null);

  // listen for token changes
  // call setUser and write new token as a cookie
  React.useEffect(() => {
    return firebase.auth().onIdTokenChanged(async (user) => {
      console.log(user);
      if (!user) {
        setUser(null);
        nookies.set(undefined, "token", "");
      } else {
        const token = await user.getIdToken();
        setUser(firebase.auth().currentUser);
        nookies.set(undefined, "token", token);
      }
    });
  }, []);

  // force refresh the token every 10 minutes
  React.useEffect(() => {
    const handle = setInterval(async () => {
      const cuser = firebase.auth().currentUser;
      if (cuser) await cuser.getIdToken(true);
    }, 10 * 60 * 1000);

    // clean up setInterval
    return () => clearInterval(handle);
  }, []);

  return (
    <AuthContext.Provider value={{ user }}>{children}</AuthContext.Provider>
  );
}
AuthProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useAuth = () => {
  return React.useContext(AuthContext);
};
